/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class MosqueDetails {
	public var _id : String?
	public var updatedAt : String?
	public var createdAt : String?
	public var name : String?
	public var googleId : String?
	public var __v : Int?
	public var photo : String?
	public var deleted : Bool?
	public var supervisorRating : Int?
	public var structureRating : Int?
	public var suppliesRating : Int?
	public var cleanlinessRating : Int?
	public var imamJoumouaRating : Int?
	public var imamKhomsRating : Int?
	public var mosque : Bool?
	public var airconditioner : Bool?
	public var hotWater : Bool?
	public var woman : Bool?
	public var comments : Array<Comments>?
	public var location : Location?
	public var rate : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let MosqueDetails_list = MosqueDetails.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of MosqueDetails Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [MosqueDetails]
    {
        var models:[MosqueDetails] = []
        for item in array
        {
            models.append(MosqueDetails(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let MosqueDetails = MosqueDetails(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: MosqueDetails Instance.
*/
	required public init?(dictionary: NSDictionary) {

		_id = dictionary["_id"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		createdAt = dictionary["createdAt"] as? String
		name = dictionary["name"] as? String
		googleId = dictionary["googleId"] as? String
		__v = dictionary["__v"] as? Int
		photo = dictionary["photo"] as? String
		deleted = dictionary["deleted"] as? Bool
		supervisorRating = dictionary["supervisorRating"] as? Int
		structureRating = dictionary["structureRating"] as? Int
		suppliesRating = dictionary["suppliesRating"] as? Int
		cleanlinessRating = dictionary["cleanlinessRating"] as? Int
		imamJoumouaRating = dictionary["imamJoumouaRating"] as? Int
		imamKhomsRating = dictionary["imamKhomsRating"] as? Int
		mosque = dictionary["mosque"] as? Bool
		airconditioner = dictionary["airconditioner"] as? Bool
		hotWater = dictionary["hotWater"] as? Bool
		woman = dictionary["woman"] as? Bool
        if (dictionary["comments"] != nil) { comments = Comments.modelsFromDictionaryArray(array: dictionary["comments"] as! NSArray) }
		if (dictionary["location"] != nil) { location = Location(dictionary: dictionary["location"] as! NSDictionary) }
		rate = dictionary["rate"] as? String
        
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self._id, forKey: "_id")
		dictionary.setValue(self.updatedAt, forKey: "updatedAt")
		dictionary.setValue(self.createdAt, forKey: "createdAt")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.googleId, forKey: "googleId")
		dictionary.setValue(self.__v, forKey: "__v")
		dictionary.setValue(self.photo, forKey: "photo")
		dictionary.setValue(self.deleted, forKey: "deleted")
		dictionary.setValue(self.supervisorRating, forKey: "supervisorRating")
		dictionary.setValue(self.structureRating, forKey: "structureRating")
		dictionary.setValue(self.suppliesRating, forKey: "suppliesRating")
		dictionary.setValue(self.cleanlinessRating, forKey: "cleanlinessRating")
		dictionary.setValue(self.imamJoumouaRating, forKey: "imamJoumouaRating")
		dictionary.setValue(self.imamKhomsRating, forKey: "imamKhomsRating")
		dictionary.setValue(self.mosque, forKey: "mosque")
		dictionary.setValue(self.airconditioner, forKey: "airconditioner")
		dictionary.setValue(self.hotWater, forKey: "hotWater")
		dictionary.setValue(self.woman, forKey: "woman")
		dictionary.setValue(self.location?.dictionaryRepresentation(), forKey: "location")
		dictionary.setValue(self.rate, forKey: "rate")

		return dictionary
	}

}
