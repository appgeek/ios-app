


import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class MosqueJson {
	public var mosques : Array<Mosques>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let MosqueJson_list = MosqueJson.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of MosqueJson Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [MosqueJson]
    {
        var models:[MosqueJson] = []
        for item in array
        {
            models.append(MosqueJson(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let MosqueJson = MosqueJson(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: MosqueJson Instance.
*/
	required public init?(dictionary: NSDictionary) {

        if (dictionary["mosques"] != nil) { mosques = Mosques.modelsFromDictionaryArray(array: dictionary["mosques"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()


		return dictionary
	}

}
