//
//  API.swift
//  IslamicApp
//
//  Created by Hamza on 12/2/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import Foundation
import Alamofire
class APIManager {
    
    static let shared = APIManager()
    
    private let mainURL = "http://54.37.136.128:3001/"
    
    private let sessionManager:SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 5
        configuration.timeoutIntervalForRequest = 5
        
        return SessionManager(configuration: configuration)
    }()
    func getMosque(onComplition:@escaping (([String:AnyObject]?,AddCommentResult)->())){

        sessionManager.request(mainURL+"mosques", method: .put, parameters: ["lng":"\(10.099923759708274)","lat":"\(35.679908562484826)","fcmtoken":FCMTOKEN], headers: ["Content-Type":"application/x-www-form-urlencoded"]).responseJSON { (dataResponse:DataResponse<Any>) in

            guard let statusCode = dataResponse.response?.statusCode else {
                onComplition(nil,.unknownError)
                return
            }
            let result = dataResponse.result.value

            switch statusCode {
            case 200:
                print("jsooon\(dataResponse.value)")
                let data = dataResponse.value as! [String:AnyObject]
                onComplition(data,.succes)
            case 400:
                let errorCode = (result as! [String:AnyObject])["code"] as! Int

                switch errorCode {
                case 117:
                    onComplition(nil,.textCannotBeEmpty)
                case 118:
                    onComplition(nil,.idCannotBeEmpty)
                case 161:
                    onComplition(nil,.castError)
                case 163:
                    onComplition(nil,.feedNotFound)
                default:
                    onComplition(nil,.unknownError)
                }

            case 500:
                onComplition(nil,.serverError)
            default:
                onComplition(nil,.unknownError)
            }

        }
    }
    
    func getMosqueById(id:String,onComplition:@escaping (([String:AnyObject]?,AddCommentResult)->())){
        
        sessionManager.request(mainURL+"mosque/\(id)", method: .get, parameters: [:], headers: ["Content-Type":"application/x-www-form-urlencoded"]).responseJSON { (dataResponse:DataResponse<Any>) in
            
            guard let statusCode = dataResponse.response?.statusCode else {
                onComplition(nil,.unknownError)
                return
            }
            let result = dataResponse.result.value
            
            switch statusCode {
            case 200:
                print("jsooon\(dataResponse.value)")
                let data = dataResponse.value as! [String:AnyObject]
                onComplition(data,.succes)
            case 400:
                let errorCode = (result as! [String:AnyObject])["code"] as! Int
                
                switch errorCode {
                case 117:
                    onComplition(nil,.textCannotBeEmpty)
                case 118:
                    onComplition(nil,.idCannotBeEmpty)
                case 161:
                    onComplition(nil,.castError)
                case 163:
                    onComplition(nil,.feedNotFound)
                default:
                    onComplition(nil,.unknownError)
                }
                
            case 500:
                onComplition(nil,.serverError)
            default:
                onComplition(nil,.unknownError)
            }
            
        }
    }
    
    //MARK/ - LoadImage
    func loadImg(imgUrl:String,onComplete:@escaping (Bool,UIImage?)->() ){
        
        
        Alamofire.request(imgUrl, method: .get).response(completionHandler: { (servData:DefaultDataResponse) in
            if let imgData = servData.data {
                let img =  UIImage(data: imgData)!
                onComplete(true,img)
            }else {
                onComplete(false,nil)
            }
            
        })
        
        
        
    }
    
    
}

public enum AddCommentResult {
    case succes
    case textCannotBeEmpty
    case idCannotBeEmpty
    case feedNotFound
    case castError
    case serverError
    case networkFailer
    case unknownError
}
