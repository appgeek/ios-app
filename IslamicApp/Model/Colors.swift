//
//  Colors.swift
//  BBM
//
//  Created by Med Amine on 9/29/17.
//  Copyright © 2017 AppGeek+. All rights reserved.
//

import UIKit


class Colors {
    
    static let white = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
    static let red = UIColor(red: 227/255, green: 61/255, blue: 81/255, alpha: 1)
    static let tabBarColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    
}



