//
//  MapViewController.swift
//  IslamicApp
//
//  Created by Hamza on 12/1/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import GoogleMaps
class MapViewController: UIViewController,GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    var mosqueTab = Array<Mosques>()
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadMosque()
        mapView.delegate = self
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        
     
         let marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2D(latitude: -30.86, longitude: 145.20)
        marker2.title = "Sydney2"
        marker2.snippet = "Australia2"
        marker2.map = mapView
        // Do any additional setup after loading the view.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.navigationItem.title = "الرئسية"
        
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MosqueDetailsVC") as! MosqueDetailsVC
        for m in mosqueTab {
            if m.name! == marker.title{
                vc.thisMosque = m
                break
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
            return true
        
    }
    @objc func openMosquéDetails()  {
        
    }
    func reloadMosque() {
        APIManager.shared.getMosque { (json, result) in
            switch result {
            case .succes:
                print("*********Mosque***********")
                 print(json)
                let tab = MosqueJson(dictionary: json as! NSDictionary)
                self.mosqueTab = (tab?.mosques)!
                let camera = GMSCameraPosition.camera(withLatitude: MYLOCATION.coordinate.latitude, longitude: MYLOCATION.coordinate.longitude, zoom: 15.0)
                self.mapView.camera = camera
                self.mapView.isMyLocationEnabled = true
                for m in self.mosqueTab {
                    // Creates a marker in the center of the map.
                    let marker = GMSMarker()
                  
                    marker.position = CLLocationCoordinate2D(latitude: Double((m.location!.lat)!)!, longitude:Double((m.location!.lng)!)!)
                    print(m.name!)
                    marker.title = m.name!
                    marker.icon = #imageLiteral(resourceName: "mosque")
                   
                    marker.map = self.mapView
                    
                }
                print("*********Mosque**\(tab?.mosques?.first?.name)*********")
                
            case .textCannotBeEmpty:
                break
            case .idCannotBeEmpty:
                break
            case .castError:
                break
            case .feedNotFound:
                break
            case .serverError:
                break
            case .networkFailer:
                break
            case .unknownError:
                 print("*********Mosque***********444")
                break
            }
        

        }
    }
    

}
extension MapViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
}
}
