//
//  CustumNavigationController.swift
//  BBM
//
//  Created by Med Amine on 9/29/17.
//  Copyright © 2017 AppGeek+. All rights reserved.
//

import UIKit
import CoreLocation
var MYLOCATION = CLLocation()
class CustumNavigationController: UINavigationController,CLLocationManagerDelegate {
 let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        // For use when the app is open & in the background
        locationManager.requestAlwaysAuthorization()
        
        // For use when the app is open
        //locationManager.requestWhenInUseAuthorization()
        
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
        }
        
        navigationBar.isTranslucent = false
        navigationBar.barTintColor = Colors.red
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let slideViewController = storyboard.instantiateViewController(withIdentifier:"AdminSlideVC") as! AdminSlideVC
        let dashboardViewController = storyboard.instantiateViewController(withIdentifier:"MapViewController") as! MapViewController
        
        
        
        
        

        
        slideViewController.principale = self
        let window = UIApplication.shared.keyWindow
        let slideMenuController = ExSlideMenuController(mainViewController:self, leftMenuViewController: slideViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = dashboardViewController
        window?.rootViewController = slideMenuController
        window?.makeKeyAndVisible()
        
    }
    // Print out the location to the console
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            MYLOCATION = location
           // print("looocation:!:\(MYLOCATION.coordinate)")
        }
    }
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to deliver pizza we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

}
