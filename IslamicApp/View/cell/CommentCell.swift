//
//  CommentCell.swift
//  IslamicApp
//
//  Created by Hamza on 12/2/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var commentTxtLBL: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
