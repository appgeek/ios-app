//
//  AdminSlideVC.swift
//  App_Conges
//
//  Created by Hamza on 8/10/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
enum LeftMenu2: Int {
    case principale = 0
    case halakatEdheker
    case about
    case contact
    
}

class AdminSlideVC: UIViewController {
    
    //MARK: Static
    
    
    //MARK: Outlet
    @IBOutlet var tabView: UITableView!
    
   // @IBOutlet weak var pubLBL: LTMorphingLabel!
    
   
   
    
    
    //MARK: Variable
    var principale:CustumNavigationController!
    var halakatEdheker:CustumNavigationController!
    var about:CustumNavigationController!
    var contact:CustumNavigationController!
    
    
    var imgTab = [#imageLiteral(resourceName: "contact"),#imageLiteral(resourceName: "contact"),#imageLiteral(resourceName: "contact"),#imageLiteral(resourceName: "contact")]
    var lblTxtTab = ["الرئسية","تعريف التطبيق","دروس وحلقات الذكر","تواصل معنا"]
    
    
    //@IBOutlet weak var pubLBL: RQShineLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabView.dataSource = self
        tabView.delegate = self
        
        print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        
     
        
        
//        let shadowSize : CGFloat = 2.0
//        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
//                                                   y: -shadowSize / 2,
//                                                   width: self.logoImg.frame.size.width + shadowSize,
//                                                   height: self.logoImg.frame.size.height + shadowSize))
//        self.logoImg.layer.masksToBounds = false
//        self.logoImg.layer.shadowColor = UIColor.init(hex: "7F7F7F").cgColor
//        self.logoImg.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        self.logoImg.layer.shadowOpacity = 1
//        self.logoImg.layer.shadowPath = shadowPath.cgPath
//        logoImg.layer.shadowRadius = logoImg.frame.height/2
//
//
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    override func viewDidLayoutSubviews() {
       
    }
    
    func changeViewController(menu : LeftMenu2){
        switch menu {
        case .principale:
            
            let dash =  storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
           
            self.principale = CustumNavigationController(rootViewController: dash)
      
            principale.navigationBar.barTintColor = UIColor.init(hex:"04B4B8")
//            dashBoard.navigationBar.tintColor = UIColor.white
//            dashBoard.navigationBar.isTranslucent = false
          //  dashBoard.navigationController?.navigationBar.barStyle = UIBarStyle.black
            principale.navigationController?.navigationBar.tintColor = UIColor.white
//            principale.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.orange]
                                   self.slideMenuController()?.changeMainViewController(principale, close: true)
            
        case .halakatEdheker:
            let nv =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HalakatEdhekerVC") as! HalakatEdhekerVC
            self.halakatEdheker = CustumNavigationController(rootViewController:nv)
             halakatEdheker.navigationBar.barTintColor = UIColor.init(hex:"04B4B8")
                self.slideMenuController()?.changeMainViewController(halakatEdheker, close: true)
        case .about:
            let joursFerier =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.about = CustumNavigationController(rootViewController:joursFerier)
            about.navigationBar.barTintColor = UIColor.init(hex:"04B4B8")
            self.slideMenuController()?.changeMainViewController(about, close: true)
        case .contact:
            let nv =  storyboard?.instantiateViewController(withIdentifier: "FinichedMoubadaraVC") as! ContactVC
            self.contact = CustumNavigationController(rootViewController:nv)
            contact.navigationBar.barTintColor = UIColor.init(hex:"04B4B8")
            contact.navigationBar.backgroundColor = UIColor.init(hex:"04B4B8")
                       self.slideMenuController()?.changeMainViewController(contact, close: true)
    
 
    

            
        
        
    }
    }
    
        
    
                
    
 

}
    
//MARK: Extension

//MARK: UITableView


extension AdminSlideVC:UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension AdminSlideVC:UITableViewDataSource{
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0 {
//            return "Compte"
//        }else if section == 1 {
//            return "administration"
//            
//        }else if section == 2 {
//            return "Mise à jour"
//            
//        }else {return "Paramétres"}
//    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.lightGray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lblTxtTab.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCellAdmin") as! MenuCellAdmin
        cell.lbl.text = lblTxtTab[indexPath.row]
        cell.Img.image = imgTab[indexPath.row]
                
        
            
               return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu2(rawValue: indexPath.row){
            self.changeViewController(menu: menu)
        }

      
        

        
    
}


}

