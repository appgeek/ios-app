//
//  MosqueDetailsVC.swift
//  IslamicApp
//
//  Created by Hamza on 12/1/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import HCSStarRatingView

class MosqueDetailsVC: UIViewController {
    
    @IBOutlet weak var suppliesRating: HCSStarRatingView!
    @IBOutlet weak var cleanRating: HCSStarRatingView!
    @IBOutlet weak var supervisorRating: HCSStarRatingView!
    @IBOutlet weak var structureRating: HCSStarRatingView!
    @IBOutlet weak var imamKHomesRating: HCSStarRatingView!
    @IBOutlet weak var imamJomo3aRating: HCSStarRatingView!
    @IBOutlet weak var GlobalReview: UIView!
    @IBOutlet weak var mosquePhoto: UIImageView!
    @IBOutlet weak var mpsqueIMG: UIImageView!
    @IBOutlet weak var aireConditionIMG: UIImageView!
    @IBOutlet weak var WamenIMG: UIImageView!
    @IBOutlet weak var hotWaterIMG: UIImageView!
    @IBOutlet weak var mosqueNameLBL: UILabel!
    @IBOutlet weak var tabView: UITableView!
    var commentTable = Array<Comments>()
    var thisMosque:Mosques!
    var mosque:MosqueDetails!
    override func viewDidLoad() {
        super.viewDidLoad()
        tabView.delegate = self
        tabView.dataSource = self
        reloadMosque()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "graph"), style: .plain, target: self, action: #selector(self.showGraph))
        // Do any additional setup after loading the view.
    }
    
    
    func reloadMosque() {
        APIManager.shared.getMosqueById(id:thisMosque._id!, onComplition: { (json, result) in
       
            switch result {
            case .succes:
                print("*********Mosque***********")
                print(json)
                let tab = MosqueDetailsJson(dictionary: json as! NSDictionary)
                self.mosque = (tab?.mosque)!
                if self.mosque.airconditioner! {
                    self.aireConditionIMG.image = #imageLiteral(resourceName: "minisplit")
                }
                if self.mosque.mosque! {
                    self.mpsqueIMG.image = #imageLiteral(resourceName: "minbar2")
                }
                if self.mosque.hotWater! {
                    self.hotWaterIMG.image = #imageLiteral(resourceName: "tap2")
                }
                if self.mosque.woman! {
                    self.WamenIMG.image = #imageLiteral(resourceName: "hijab2")
                }
                APIManager.shared.loadImg(imgUrl: self.mosque.photo!, onComplete: { (succes, img) in
                    self.mosquePhoto.image = img
                })
                self.mosqueNameLBL.text = self.mosque.name!
                if self.mosque.rate! != "-1" {
                self.cleanRating.value = CGFloat(self.mosque.cleanlinessRating!)
                self.imamJomo3aRating.value = CGFloat(self.mosque.imamJoumouaRating!)
                self.imamKHomesRating.value = CGFloat(self.mosque.imamKhomsRating!)
                self.structureRating.value = CGFloat(self.mosque.structureRating!)
                self.supervisorRating.value = CGFloat(self.mosque.supervisorRating!)
                self.suppliesRating.value = CGFloat(self.mosque.suppliesRating!)
                    }
                self.commentTable = self.mosque.comments!
                self.tabView.reloadData()
                print("*********Mosque***********")
                
            case .textCannotBeEmpty:
                break
            case .idCannotBeEmpty:
                break
            case .castError:
                break
            case .feedNotFound:
                break
            case .serverError:
                break
            case .networkFailer:
                break
            case .unknownError:
                print("*********Mosque***********444")
                break
            }
            
            
        })
    }

    @objc func showGraph(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GraphVC") as! GraphVC
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.navigationItem.title = "معلومات عن المسجد"
        
    }
}
extension MosqueDetailsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cmnt = commentTable[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
        cell.commentTxtLBL.text = cmnt.text!
        cell.timeLBL.text = cmnt.date!
        return cell
    }
    
    
    
}
