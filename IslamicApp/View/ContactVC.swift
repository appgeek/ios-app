//
//  ContactVC.swift
//  Mobaderat
//
//  Created by Hamza on 9/19/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit
import Alamofire


class ContactVC: UIViewController {
    
    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!

    @IBOutlet weak var containermsgTextView: UIView!
    @IBOutlet weak var msgTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.nameTF.layer.masksToBounds = false
        self.nameTF.layer.shadowColor = UIColor.init(hex: "04B4B8").cgColor
        self.nameTF.layer.shadowOffset = CGSize(width: 3, height: 5)
        self.nameTF.layer.shadowOpacity = 0.5
        
        self.containermsgTextView.layer.masksToBounds = false
        self.containermsgTextView.layer.shadowColor = UIColor.init(hex: "04B4B8").cgColor
        self.containermsgTextView.layer.shadowOffset = CGSize(width: 3, height: 5)
        self.containermsgTextView.layer.shadowOpacity = 0.5
        containermsgTextView.layer.cornerRadius = 15
        msgTextView.layer.cornerRadius = 15
        
        self.emailTF.layer.masksToBounds = false
        self.emailTF.layer.shadowColor = UIColor.init(hex: "04B4B8").cgColor
        self.emailTF.layer.shadowOffset = CGSize(width: 3, height: 5)
        self.emailTF.layer.shadowOpacity = 0.5

        // Do any additional setup after loading the view.
    }


    override func viewDidAppear(_ animated: Bool) {
        //scrolView.setContentOffset(CGPoint(x:0,y:0), animated: false)
    }
override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
}

override func viewWillAppear(_ animated: Bool) {
    self.setNavigationBarItem()
    self.navigationItem.title = "تواصل معنا"
}

    @IBAction func txtEmailEditingChanged(_ sender: UITextField) {
//        
//        if sender.text != nil && self.verfyEmail(candidate: sender.text!) {
//            sender.textColor = UIColor.black
//        }else{
//            sender.textColor = UIColor.red
//        }
//        
        
    }

    
    @IBAction func sendPressed(_ sender: Any) {
        
//        if (self.nameTF.text == "" || self.emailTF.text == "" || self.msgTextView.text == ""){
//            SCLAlertView().showWarning("", subTitle: "الرجاء تعمير كل الخانات")
//
//
//        }else{
//
//        self.startLoading(message: "جاري تحميل رسالتك...")
//
//        Alamofire.request("\(API.BASE_URL)/user/contactus", method: .put, parameters: ["name":self.nameTF.text!,"email":emailTF.text!,"text":msgTextView.text!], headers: ["Content-Type":"application/x-www-form-urlencoded","Authorization":API.token]).responseJSON { (response) in
//            guard response.result.isSuccess else {
//
//                SCLAlertView().showTitle("",subTitle: "عملية الارسال لم تكتمل",style: SCLAlertViewStyle.error,closeButtonTitle: "تم",colorStyle: 0xFF0000,colorTextButton: 0xFFFFFF)
//
//                self.stopLoading(succes: false)
//                return
//            }
//
//            if response.response?.statusCode == 400 {
//                let index = response.result.value as! [String:Any]
//                let errorIndex = index["error"] as! Int
//                StaticVariable.traiteError(ErrorIndex: errorIndex)
//            }
//
//            self.nameTF.text = ""
//            self.emailTF.text = ""
//            self.msgTextView.text = ""
//            self.stopLoading(succes: true)
//
//    }
//        }
}

}


//Mark:-Extension
extension ContactVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
    
    
    
}
