//
//  GraphVC.swift
//  IslamicApp
//
//  Created by Hamza on 12/2/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

class GraphVC: UIViewController {

    @IBOutlet weak var graphView: ScrollableGraphView!
    
    private var graphViewDatas:[Double] = [1, 2,3,4,5]
    private var graphViewLables:[String] = ["15","30","45","","60"]
    override func viewDidLoad() {
        super.viewDidLoad()
 graphView.set(data: graphViewDatas, withLabels: graphViewLables)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
