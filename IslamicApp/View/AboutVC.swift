//
//  AboutVC.swift
//  Mobaderat
//
//  Created by Hamza on 9/21/17.
//  Copyright © 2017 Hamza. All rights reserved.
//

import UIKit

import BPBlockActivityIndicator
class AboutVC: UIViewController{
    @IBOutlet weak var textDescriptionLBL: UILabel!

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var blockIndicator: BPBlockActivityIndicator!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
            // playerView.alpha = 0
        
        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style:.plain, target:nil, action:nil)
        self.blockIndicator.animate()
//        API.getEditable(type: "introduction") { (succes:Bool, json:[String : AnyObject]?) in
//            if succes {
//               let  editable = EditableJson(dictionary: json! as NSDictionary)
//                var  youtubeURL = ""
//                if editable?.editable?.video != nil {
//                    youtubeURL = (editable?.editable?.video!)!
//
//
//                self.playerView.delegate = self
//
//                let playerVars = ["playsinline": 1] // 0: will play video in fullscreen
//                self.playerView.load(withVideoId: self.getYoutubeId(youtubeUrl: youtubeURL), playerVars: playerVars)
//                }
//                if editable?.editable?.image != nil {
//                    API.loadImg(imgUrl: (editable?.editable?.image)!, onComplete: { (succes, img) in
//                        self.imgView.image = img
//                    })
//                }
//               self.textDescriptionLBL.text = editable?.editable?.text!
//                self.blockIndicator.stop()
//                self.blockIndicator.alpha = 0
//
//            }else {
//                self.blockIndicator.stop()
//                self.blockIndicator.alpha = 0
//
//            }
//        }
    }

    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func youtubePressed(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://www.youtube.com/channel/UCM0KeDPhMBRNxKuSFhUGdUg")! as URL)

    }
    @IBAction func twitterPressed(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://twitter.com/mobaderat?s=09")! as URL)

    }
    @IBAction func instagramPressed(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://www.instagram.com/mobaderat0/")! as URL)

    }
    
    
}
